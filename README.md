# BeatSaberPlaylist

Our custom playlist for beatsaber

## Setup

To setup you need to put the `GaradosAndEaglesList.bplist` to your BeatSaber installation. For that they are the following options:

### Copy of file

Copy the File `GaradosAndEaglesList.bplist` to `.../steamapps/common/Beat Saber/Playlist/GaradosAndEaglesList.bplist`.

### Windows Linking

1. Clone the Project to the **same** drive as your BeatSaber installation
2. Open the Console as Adminstrator:
    1. Open Windows Menu
    2. Enter `cmd`
    3. Select the Windows Console and start as Administrator
3. Enter the command:
    ```sh
    mklink "Drive:\...\steamapps\common\Beat Saber\Playlists\GaradosAndEaglesList.bplist" "Drive:\...\beatsaberplaylist\GaradosAndEaglesList.bplist"
    ```

### Linux Linking

*This needs some testing*

1. Clone the Project
2. Open the Console
3. Enter the Command:
    ```sh
    mklink ".../steamapps/common/Beat Saber/Playlists/GaradosAndEaglesList.bplist" ".../beatsaberplaylist/GaradosAndEaglesList.bplist"
    ```
## Editing

Your need some tools for editing Beat Saber Playlist. The following are recommended:

- [https://github.com/Alaanor/beatlist](https://github.com/Alaanor/beatlist) (Windows, possibly Linux)

### Contributing

The Playlist files need a pre process before it can be uploaded to git. Therefore you need to setup the following:

1. Install Python in your global PATH
2. open the console
3. Enter
	```sh
	cd .../beatsaberplaylist/
	git config --local include.path ../.gitconfig
	```

This will ensure that clean files will be uploaded that are optimized for git.

## Using Playlist

You need a tool to download the Playlist content. The following are recommended:

- [https://github.com/Alaanor/beatlist](https://github.com/Alaanor/beatlist) (Windows, possibly Linux)

To show the playlist in Beat Saber you need to install the following mod:
- [rithik-b/PlaylistLoaderPlugin](https://github.com/rithik-b/PlaylistLoaderPlugin) (Avaible in ModAssistant)
